// tạo message thông báo khi có lỗi
function showmessage(idSpan, message){

    document.getElementById(idSpan).innerHTML = message;
    document.getElementById(idSpan).style.display = "inline-block";
}

    
// kiểm tra tài khoản
function kiemTraTaiKhoan(idNv){
    if(idNv === ""){
        showmessage("tbTKNV", "ID không được để trống")
        return false;
    }
    if (!/^\d{4,6}$/.test(idNv)) {
        showmessage("tbTKNV", "Tài khoản phải gồm từ 4-6 ký số.");
        return false;
    }
    showmessage("tbTKNV", "");
    return true;
}

// kiểm tra tên nhân viên
function kiemTraTenNV(name){
    // Kiểm tra ràng buộc Tên nhân viên không để trống và phải là chữ
   if(name == ""){
    showmessage("tbTen", "Tên nhân viên không được để trống");
        return false;
   }
   if (!/^[a-zA-ZÀ-ỹ\s]+$/.test(name)) {
    showmessage("tbTen", "Tên nhân viên phải là chữ.");
        return false;
  }
    showmessage("tbTen", "");
        return true;    

}

// kiểm tra email
function kiemTraEmail(email){
    // kiểm tra Email không được để trống và phải đúng định dạng
    if (email == "") {
        showmessage("tbEmail", "Email không được để trống.");
        return false;
    }
    if (!/^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/.test(email)) {
        showmessage("tbEmail", "Email không đúng định dạng.");
        return false;
    }
    showmessage("tbEmail", "");
        return true;
}

// kiểm tra mật khẩu
function kiemTraMatKhau(password){
    // Kiểm tra ràng buộc Mật khẩu không để trống và từ 6-10 ký tự (bao gồm ít nhất 1 số, 1 chữ hoa, 1 ký tự đặc biệt)
  if (password == "") {
    showmessage("tbMatKhau", "Mật khẩu không được để trống.");
    return false;
  }
  if (!/^.*(?=.{6,10})(?=.*\d)(?=.*[A-Z])(?=.*[!@#$%^&]).*$/.test(password)) {
    showmessage("tbMatKhau", "Mật khẩu phải từ 6-10 ký tự và chứa ít nhất 1 số, 1 chữ hoa và 1 ký tự đặc biệt");
    return false;
    }
    showmessage("tbMatKhau", "");
    return true;
}

// kiểm tra ngày làm
function kiemTraNgayLam(ngaylam) {
    // Kiểm tra không để trống
    if (ngaylam == "") {
      showmessage("tbNgay", "Ngày làm không được để trống")
      return false;
    }
    
    // Kiểm tra định dạng mm/dd/yyyy
    var regex = /^\d{2}\/\d{2}\/\d{4}$/;
    if (!regex.test(ngaylam)) {
        showmessage("tbNgay", "Sai định dạng ngày mm/dd/yyyy");
      return false;
    }
     // Kiểm tra ngày hợp lệ
    var parts = ngaylam.split("/");
    var day = parseInt(parts[1], 10);
    var month = parseInt(parts[0], 10);
    var year = parseInt(parts[2], 10);
    if (isNaN(day) || isNaN(month) || isNaN(year)) {
        showmessage("tbNgay", "Ngày làm phải là số nguyên");
       return false;
    }

    var currentDate = new Date();
    var inputDate = new Date(year, month - 1, day);
    if (inputDate > currentDate) {
        showmessage("tbNgay", "Ngày làm không hợp lệ")
       return false;
    }
    return true;
}

// kiểm tra lương cơ bản
function kiemTraLuongCoBan(luongCB) {
    // Kiểm tra không để trống
    if (luongCB == "") {
      showmessage("tbLuongCB", "Lương không được để trống");
      return false;
    }
    
    // Kiểm tra giá trị trong khoảng từ 1,000,000 đến 20,000,000
    var luong = parseInt(luongCB, 10);
    if (isNaN(luong) || luong < 1000000 || luong > 20000000) {
      showmessage("tbLuongCB", "Bạn nhập sai lương");
      return false;
    }
    showmessage("tbLuongCB", "");
    return true;
}


function kiemTraChucVu(chucvu) {
    
    // Kiểm tra chọn chức vụ hợp lệ
    var chucVuHopLe = ["Sếp", "Trưởng phòng", "Nhân viên"];
    if (!chucVuHopLe.includes(chucvu)) {
        showmessage("tbChucVu", " Chức vụ không hợp lệ");
      return false;
    }
    showmessage("tbChucVu", "");
    return true;
}

// kiểm tra giờ làm
function kiemTraSoGioLam(giolam) {
    // Kiểm tra không để trống
    if (giolam == "") {
        showmessage("tbGiolam", " Giờ làm không được để trống");
      return false;
    }
    
    // Kiểm tra giá trị trong khoảng từ 80 đến 200
    var gio = parseInt(giolam, 10);
    if (isNaN(gio) || gio < 80 || gio > 200) {
      showmessage("tbGiolam", " Giờ làm tối thiểu là 80 giờ và tối đa là 200 giờ" );
      return false;
    }
    showmessage("tbGiolam", "");
    return true;
}