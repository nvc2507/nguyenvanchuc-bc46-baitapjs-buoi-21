var dsnv = [];
var dataJson = localStorage.getItem("DSNV");

if(dataJson != null){
    dsnv = JSON.parse(dataJson).map(function(item){
        return new NhanVien(
            item.msnv,
            item.name,
            item.email,
            item.password,
            item.ngaylam,
            item.luongCB,
            item.chucvu,
            item.giolam,
            item.tongLuong,
            item.xepLoai);
    });
   
    renderDSNV(dsnv);
}

// thêm nhân viên
function themNV(){
    var nv = layThongTinNhanVien();

    var isValid = true;
  
    isValid = kiemTraTaiKhoan(nv.msnv, dsnv) && isValid;
  
    // Kiểm tra tên nhân viên
    isValid = kiemTraTenNV(nv.name) && isValid;
    // Kiểm tra Email
     isValid = kiemTraEmail(nv.email) && isValid;
    // kiểm tra mật khẩu
     isValid = kiemTraMatKhau(nv.password) && isValid;
    // kiểm tra ngày làm
    isValid = kiemTraNgayLam(nv.ngaylam) && isValid;
    // kiểm tra Lương
    isValid = kiemTraLuongCoBan(nv.luongCB) && isValid;
    // kiểm tra chức vụ
    isValid = kiemTraChucVu(nv.chucvu) && isValid;
    //kiểm tra giờ làm
    isValid = kiemTraSoGioLam(nv.giolam) && isValid;
  
    if(isValid){

        dsnv.push(nv);

        renderDSNV(dsnv);// render dsnv

        // reset input
        resetInput();

        //Lưu DSNV vào localStorage
        var dataJson = JSON.stringify(dsnv);
        localStorage.setItem("DSNV", dataJson);
    }
  
}

//Xóa NV

function xoaNV(id){
    var index = dsnv.findIndex(function(item){
        return item.msnv == id;
    });
    dsnv.splice(index,1);
    renderDSNV(dsnv);

    var dataJson = JSON.stringify(dsnv);
    localStorage.setItem("DSNV", dataJson);
}
// sửa và cập nhật thông tin nhân viên
function suaNV(id){
    var index = dsnv.findIndex(function(item){
        return item.msnv == id;
    });
    showThongTinNV(dsnv[index]);

    $('#myModal').modal('show');
};

// Cập nhật nhân viên
function capNhatNV() {
    // Lấy thông tin từ form
    var nv = layThongTinNhanVien();
    
    // Tìm vị trí của nhân viên trong danh sách dsnv
    var index = dsnv.findIndex(function(item) {
        return item.msnv == nv.msnv;
    });

    // Cập nhật thông tin nhân viên khi được sửa
    dsnv[index].name = nv.name;
    dsnv[index].email = nv.email;
    dsnv[index].password = nv.password;
    dsnv[index].ngaylam = nv.ngaylam;
    dsnv[index].luongCB = nv.luongCB;
    dsnv[index].chucvu = nv.chucvu;
    dsnv[index].giolam = nv.giolam;

    var isValid = true;
  
    // Kiểm tra tên nhân viên
    isValid = kiemTraTenNV(nv.name) && isValid;
    // Kiểm tra Email
     isValid = kiemTraEmail(nv.email) && isValid;
    // kiểm tra mật khẩu
     isValid = kiemTraMatKhau(nv.password) && isValid;
    // kiểm tra ngày làm
    isValid = kiemTraNgayLam(nv.ngaylam) && isValid;
    // kiểm tra Lương
    isValid = kiemTraLuongCoBan(nv.luongCB) && isValid;
    // kiểm tra chức vụ
    isValid = kiemTraChucVu(nv.chucvu) && isValid;
    //kiểm tra giờ làm
    isValid = kiemTraSoGioLam(nv.giolam) && isValid;
    
   if(isValid){
        renderDSNV(dsnv); // Render lại danh sách nhân viên
    
        var dataJson = JSON.stringify(dsnv);
        localStorage.setItem("DSNV", dataJson); // Cập nhật DSNV mới vào localStorage
    
        // Reset form input
        resetInput();
    }
}

// tìm nhân viên theo xếp loại
function timNV() {
    var inputLoaiNV = document.getElementById("searchName").value;
    var nhanVienTheoLoai = dsnv.filter(function(nv) {
        return nv.xepLoai() === inputLoaiNV;
    });
    renderDSNV(nhanVienTheoLoai);
};
