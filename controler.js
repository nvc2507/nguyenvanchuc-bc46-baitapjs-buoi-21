//model nhân viên
function NhanVien(
    msnv,
    name,
    email,
    password,
    ngaylam,
    luongCB,
    chucvu,
    giolam) {
    this.msnv = msnv;
    this.name = name;
    this.email = email;
    this.password = password;
    this.ngaylam = ngaylam;
    this.luongCB = luongCB;
    this.chucvu = chucvu;
    this.giolam = giolam;
    this.tongLuong = function() {
        
        var luong = 0;
        if(this.chucvu === 'Sếp'){
            luong = this.luongCB*3;
        }else if(this.chucvu === 'Trưởng phòng'){
            luong = this.luongCB*2;
        }else if(this.chucvu === 'Nhân viên'){
            luong = this.luongCB*1;
        }
        return luong;
    };
    this.xepLoai = function(){
        var xeploai = "";
        if(this.giolam < 160){
            xeploai = "Trung bình";
        }else if(this.giolam >= 192){
            xeploai = "Xuất sắc"
        }else if(this.giolam >= 176){
            xeploai = "Giỏi";
        }else if(this.giolam >= 160){
            xeploai = "Khá";
        }
        return xeploai;
    };

    


}
//render dsnv
function renderDSNV(dsnv){
    var contentHTML = "";
    for(var i = 0; i < dsnv.length; i++){
        var nv = dsnv[i];
        var content = `<tr>
        <td>${nv.msnv}</td>
        <td>${nv.name}</td>
        <td>${nv.email}</td>
        <td>${nv.ngaylam}</td>
        <td>${nv.chucvu}</td>
        <td>${nv.tongLuong()}</td>
        <td>${nv.xepLoai()}</td>
        <td>
        <button onclick="xoaNV('${nv.msnv}')" class="btn btn-danger">Xóa</button>
        <button onclick="suaNV('${nv.msnv}')" class="btn btn-warning">Sửa</button>
        </td>
        </tr>`;
        contentHTML += content;
    }

    document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

// lấy thông tin nhân viên.
function layThongTinNhanVien(){
    var msnv =   document.getElementById("tknv").value;
    var name = document.getElementById("name").value;
    var email = document.getElementById("email").value;
    var password = document.getElementById("password").value;
    var ngaylam = document.getElementById("datepicker").value;
    var luongCB = document.getElementById("luongCB").value*1;
    var chucvu = document.getElementById("chucvu").value;
    var giolam = document.getElementById("gioLam").value*1;
    // tạo NV
    return new NhanVien(
        msnv,
        name,
        email,
        password,
        ngaylam,
        luongCB,
        chucvu,
        giolam);
}

// show thông tin nhân viên lên form khi sửa
function showThongTinNV(nv){
    document.getElementById("tknv").value = nv.msnv;
    document.getElementById("name").value =nv.name;
    document.getElementById("email").value = nv.email;
    document.getElementById("password").value = nv.password;
    document.getElementById("datepicker").value = nv.ngaylam;
    document.getElementById("luongCB").value = nv.luongCB;
    document.getElementById("chucvu").value = nv.chucvu;
    document.getElementById("gioLam").value = nv.giolam;
    
}

// hàm để reset input
function resetInput() {
    document.getElementById("tknv").value = "";
    document.getElementById("name").value = "";
    document.getElementById("email").value = "";
    document.getElementById("password").value = "";
    document.getElementById("datepicker").value = "";
    document.getElementById("luongCB").value = "";
    document.getElementById("chucvu").value = "";
    document.getElementById("gioLam").value = "";
}